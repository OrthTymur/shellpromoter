build:
	@$(MAKE) -C app/ build
	@$(MAKE) -C client/ build

test:
	@$(MAKE) -C app/ test

deploy: build
	@./bin/shellpromoter
